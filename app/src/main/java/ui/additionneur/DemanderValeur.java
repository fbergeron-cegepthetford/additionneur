package ui.additionneur;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DemanderValeur extends AppCompatActivity
        implements View.OnClickListener {
    private EditText etNombre;
    private Button btEnvoyer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initialiserVue();
    }

    private void initialiserVue() {
        this.setContentView(R.layout.activity_demander_valeur);
        etNombre = (EditText) this.findViewById(R.id.etNombre);
        btEnvoyer = (Button) this.findViewById(R.id.btEnvoyer);

        btEnvoyer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btEnvoyer:
                try {
                    int nombre = new Integer(etNombre.getText().toString()).intValue();
                    Bundle panier = new Bundle();
                    Intent retour = new Intent();
                    panier.putInt("nombre", nombre);
                    retour.putExtras(panier);
                    this.setResult(RESULT_OK, retour);
                    this.finish();
                } catch (Exception e) {
                    Toast toast = Toast.makeText(this, ""
                                    + etNombre.getText().toString()
                                    + ' '
                                    + this.getResources().getString(R.string.mauvaisEntier)
                            , Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
                break;
        }
    }
}
