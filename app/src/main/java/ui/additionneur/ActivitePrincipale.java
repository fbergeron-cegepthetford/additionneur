package ui.additionneur;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import affaire.additionneur.Additionneur;


public class ActivitePrincipale extends AppCompatActivity
        implements View.OnClickListener {
    private Additionneur additionneur;
    private TextView tvResultat;
    private Button btAjouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initialiserVue();
    }

    private void initialiserVue() {
        this.setContentView(R.layout.activity_activite_principale);
        tvResultat = (TextView) this.findViewById(R.id.tvResultat);
        btAjouter = (Button) this.findViewById(R.id.btAjouter);
        additionneur = new Additionneur();

        this.mettreAJourVue();

        btAjouter.setOnClickListener(this);
    }

    private void mettreAJourVue() {
        tvResultat.setText("" + additionneur.getValeur());
    }

    private void demanderValeur() {
        Intent intent = new Intent(this, DemanderValeur.class);
        this.startActivityForResult(intent, 0);  // Le code de requête est 0
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btAjouter:
                this.demanderValeur();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) // Seul cas possible dans ce programme
            if (resultCode == RESULT_OK) {
                Bundle panier = data.getExtras();
                int nombre = panier.getInt("nombre");
                additionneur.additionner(nombre);
                this.mettreAJourVue();
            }
    }
}
