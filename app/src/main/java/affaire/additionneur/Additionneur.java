package affaire.additionneur;

public class Additionneur {
    private int valeur;

    public Additionneur() {
        this(0);
    }

    public Additionneur(int valeur) {
        this.setValeur(valeur);
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public int getValeur() {
        return valeur;
    }

    public void additionner(int nombre) {
        valeur += nombre;
    }
}
